ecllib_audio_def_libstart	= "-------- LIB START --------";
ecllib_audio_def_libname	= "ECL_GMSJS_LIB_AUDIO";
ecllib_audio_def_libver		= "0.7.0";
ecllib_audio_def_copyright	= "Copyright 2014 by Emily&Charlotte Lounge";

ecllib_audio_var_srcmax		= 10;										// max sources
ecllib_audio_var_srcsw		= new Array(ecllib_audio_var_srcmax);		// used flag -> 0:free/1:used
ecllib_audio_var_srcpri		= new Array(ecllib_audio_var_srcmax);		// priority -> 0:high to 999:low
ecllib_audio_var_srctype	= new Array(ecllib_audio_var_srcmax);		// audio type -> 0:BGM/1:VOICE/2:SE
ecllib_audio_var_srcmode	= new Array(ecllib_audio_var_srcmax);		// play overwrite -> 1:force/2:skip
ecllib_audio_var_srcsrc		= new Array(ecllib_audio_var_srcmax);		// audio source file
ecllib_audio_var_srcready	= new Array(ecllib_audio_var_srcmax);		// status -> 0:not ready/1:loading/2:ready/-1:error
ecllib_audio_var_obj		= new Array(ecllib_audio_var_srcmax);		// audio object

ecllib_audio_var_max		= 4;										// max player objects
ecllib_audio_var_sw			= new Array(ecllib_audio_var_max);			// used flag -> -1:free/0~:src id
ecllib_audio_var_loop		= new Array(ecllib_audio_var_max);			// loop -> 0:no/1:loop
ecllib_audio_var_vol		= new Array(ecllib_audio_var_max);			// volume -> 0.0 to 1.0
ecllib_audio_var_rate		= new Array(ecllib_audio_var_max);			// playback rate

ecllib_audio_var_format		= "";										// audio type -> mp3 or ogg
ecllib_audio_var_init		= 0;										// is init -> 0:no/1:ok
ecllib_audio_var_loadcnt	= 0;										// pre-load count

ecllib_audio_var_vol_m		= 1.0;										// master volume
ecllib_audio_var_vol_b		= 0.8;										// BGM volume
ecllib_audio_var_vol_v		= 1.0;										// VOICE volume
ecllib_audio_var_vol_s		= 0.9;										// SE volume

// init
function ecllib_audio_init() {
	ecllib_audio_var_init = 0;
	ecllib_audio_var_loadcnt = 0;
	ecllib_audio_var_format = "";
	var tmp_audio = null;
	try {
		tmp_audio = new Audio("");
		if(tmp_audio.canPlayType) {
			if((tmp_audio.canPlayType("audio/mpeg") != "")&&(tmp_audio.canPlayType("audio/mpeg") != "no")) {
				ecllib_audio_var_format = "mp3";
			}
			else if((tmp_audio.canPlayType("audio/ogg") != "")&&(tmp_audio.canPlayType("audio/ogg") != "no")) {
				ecllib_audio_var_format = "ogg";
			}
		}
	}
	catch(e) {
	}
	tmp_audio = null;
	var i = 0;
	for(i=0; i<ecllib_audio_var_srcmax; i++) ecllib_audio_initsrc(i);
	for(i=0; i<ecllib_audio_var_max; i++) ecllib_audio_initobj(i);
	ecllib_audio_var_init = 1;
	return ecllib_audio_var_format;
}
function ecllib_audio_get_format() {
	return ecllib_audio_var_format;
}

// destroy
function ecllib_audio_destroy() {
	return ecllib_audio_stopall();
}

// master volume
function ecllib_audio_set_volMaster(argument0) {
	ecllib_audio_var_vol_m = argument0;
	return ecllib_audio_rewrite_vol();
}
function ecllib_audio_get_volMaster() {
	return ecllib_audio_var_vol_m;
}

// BGM volume
function ecllib_audio_set_volBGM(argument0) {
	ecllib_audio_var_vol_b = argument0;
	return ecllib_audio_rewrite_vol();
}
function ecllib_audio_get_volBGM() {
	return ecllib_audio_var_vol_b;
}

// VOICE volume
function ecllib_audio_set_volVOICE(argument0) {
	ecllib_audio_var_vol_v = argument0;
	return ecllib_audio_rewrite_vol();
}
function ecllib_audio_get_volVOICE() {
	return ecllib_audio_var_vol_v;
}

// SE volume
function ecllib_audio_set_volSE(argument0) {
	ecllib_audio_var_vol_s = argument0;
	return ecllib_audio_rewrite_vol();
}
function ecllib_audio_get_volSE() {
	return ecllib_audio_var_vol_s;
}

// init source
function ecllib_audio_initsrc(argument0) {
	var i = argument0;
	ecllib_audio_var_srcsw[i] = 0;
	ecllib_audio_var_srcpri[i] = 0;
	ecllib_audio_var_srctype[i] = 0;
	ecllib_audio_var_srcmode[i] = 0;
	ecllib_audio_var_srcsrc[i] = "";
	ecllib_audio_var_srcready[i] = 0;
	ecllib_audio_var_obj[i] = null;
	return 0;
}

// init player object
function ecllib_audio_initobj(argument0) {
	var i = argument0;
	ecllib_audio_var_sw[i] = -1;
	ecllib_audio_var_loop[i] = 0;
	ecllib_audio_var_vol[i] = 1.0;
	ecllib_audio_var_rate[i] = 1.0;
	return 0;
}

// source load
// argument0 = buffer id
// argument1 = file
// argument2 = priority
// argument3 = audio type
// argument4 = play over write
function ecllib_audio_load(argument0, argument1, argument2, argument3, argument4) {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	var i = argument0;
	if(i >= ecllib_audio_var_srcmax) return -1;
	for(var j=0; j<ecllib_audio_var_max; j++) {
		if(ecllib_audio_var_sw[j] == argument0) ecllib_audio_stop(j);
	}
	ecllib_audio_var_srcpri[i] = argument2;
	ecllib_audio_var_srctype[i] = argument3;
	ecllib_audio_var_srcmode[i] = argument4;
	ecllib_audio_var_srcsrc[i] = argument1 + "." + ecllib_audio_var_format;
	ecllib_audio_var_srcready[i] = 1;
	ecllib_audio_var_loadcnt++;
	if(ecllib_audio_var_srcsw[i] == 0) {
		ecllib_audio_var_srcsw[i] = 1;
		ecllib_audio_var_obj[i] = document.createElement("audio");
		ecllib_audio_var_obj[i].autoplay = false;
		ecllib_audio_var_obj[i].addEventListener("canplaythrough", function() { ecllib_audio_loadready(i); }, false);
		ecllib_audio_var_obj[i].addEventListener("ended", function() { ecllib_audio_playend(i); },false); 
	}
	else ecllib_audio_var_obj[i].autoplay = false;
	ecllib_audio_var_obj[i].src = ecllib_audio_var_srcsrc[i];
	return i;
}
function ecllib_audio_loadready(objnum) {
	ecllib_audio_var_srcready[objnum] = 2;
	ecllib_audio_var_loadcnt--;
}
function ecllib_audio_playend(objnum) {
	var r = 0;
	for(var i=0; i<ecllib_audio_var_max; i++) {
		if(ecllib_audio_var_sw[i] == objnum) {
			if(ecllib_audio_var_loop[i] == 0) ecllib_audio_initobj(i);
			else if(r == 0) {
				r = 1;
				ecllib_audio_var_obj[objnum].play();
			}
		}
	}
}

// pre-load check
function ecllib_audio_check_ready() {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	return ecllib_audio_var_loadcnt;
}

// stop
function ecllib_audio_stop(argument0) {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	var i = argument0;
	if(ecllib_audio_var_sw[i] < 0) return 0;
	var j = ecllib_audio_var_sw[i];
	ecllib_audio_var_obj[j].volume = 0.0;
	if(!ecllib_audio_var_obj[j].ended) {
		ecllib_audio_var_obj[j].pause();
		ecllib_audio_var_obj[j].currentTime = 0;
	}
	ecllib_audio_initobj(i);
	return 0;
}

// stop all
function ecllib_audio_stopall() {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	for(var i=0; i<ecllib_audio_var_max; i++) ecllib_audio_stop(i);
	return 0;
}

// stop selected type
function ecllib_audio_stopseltype(argument0) {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	for(var i=0; i<ecllib_audio_var_max; i++) if((ecllib_audio_var_sw[i] >= 0)&&(ecllib_audio_var_srctype[ecllib_audio_var_sw[i]] == argument0)) ecllib_audio_stop(i);
	return 0;
}

// stop BGM
function ecllib_audio_stopBGM() {
	return ecllib_audio_stopseltype(0);
}

// stop VOICE
function ecllib_audio_stopVOICE() {
	return ecllib_audio_stopseltype(1);
}

// stop SE
function ecllib_audio_stopSE() {
	return ecllib_audio_stopseltype(2);
}

// play
// argument0 = source id
// argument1 = loop(0:false/1:true)
// argument2 = volume
// argument3 = rate
function ecllib_audio_play(argument0, argument1, argument2, argument3) {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	var i = argument0;
	if((ecllib_audio_var_srcsw[i] < 1)||(ecllib_audio_var_srcready[i] < 2)) return -1;
	var k = -1;
	var h = -1;
	var g = -1;
	var f = ecllib_audio_var_srcpri[i];
	var j = 0;
	if(ecllib_audio_var_srcmode[i] > 0) {
		for(j=0; j<ecllib_audio_var_max; j++) {
			if(ecllib_audio_var_sw[j] == i) k = j;
			else if(ecllib_audio_var_sw[j] < 0) h = j;
			else if(ecllib_audio_var_srcpri[ecllib_audio_var_sw[j]] > f) g = j;
		}
	}
	if((ecllib_audio_var_srcmode[i] == 2)&&(k >= 0)) return -1;
	if((ecllib_audio_var_srcmode[i] == 1)&&(k >= 0)) h = k;
	if((h < 0)&&(g >= 0)) h = g;
	if(h < 0) return -1;
	var v = ecllib_audio_calc_vol(i, argument0);
	ecllib_audio_var_obj[i].volume = 0.0;
	ecllib_audio_var_obj[i].load();
	if(argument1 == 0) ecllib_audio_var_obj[i].loop = false;
	else ecllib_audio_var_obj[i].loop = true;
	ecllib_audio_var_obj[i].playbackRate = argument3;
	ecllib_audio_var_obj[i].play();
	ecllib_audio_var_obj[i].volume = v;
	ecllib_audio_var_sw[h] = i;
	ecllib_audio_var_loop[h] = argument1;
	ecllib_audio_var_vol[h] = argument2;
	ecllib_audio_var_rate[h] = argument3;
	return h;
}

// volume rewrite
function ecllib_audio_rewrite_vol() {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	for(var i=0; i<ecllib_audio_var_max; i++) ecllib_audio_change_vol(i);
	return 0;
}

// volume change
function ecllib_audio_change_vol(argument0) {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	var i = argument0;
	if(ecllib_audio_var_sw[i] < 0) return -1;
	ecllib_audio_var_vol[i] = argument0;
	var j = ecllib_audio_var_sw[i];
	var v = ecllib_audio_calc_vol(j, argument0);
	ecllib_audio_var_obj[j].volume = v;
	return 0;
}

// calc volume
// argument0 = source id
// argument1 = new volume
function ecllib_audio_calc_vol(argument0, argument1) {
	var i = argument0;
	var v = ecllib_audio_var_vol_b;
	if(ecllib_audio_var_srctype[i] == 1) v = ecllib_audio_var_vol_v;
	else if(ecllib_audio_var_srctype[i] == 2) v = ecllib_audio_var_vol_s;
	v = argument1 * v * ecllib_audio_var_vol_m;
	if(v > 1.0) v = 1.0;
	else if(v < 0.0) v = 0.0;
	return v;
}

// playback rate change
// argument0 = player id
// argument1 = new rate
function ecllib_audio_change_rate(argument0, argument1) {
	if((ecllib_audio_var_init == 0)||(ecllib_audio_var_format == "")) return -1;
	var i = argument0;
	if(ecllib_audio_var_sw[i] < 0) return -1;
	ecllib_audio_var_rate[i] = argument1;
	var j = ecllib_audio_var_sw[i];
	ecllib_audio_var_obj[j].playbackRate = argument1;
	return 0;
}

ecllib_audio_def_libend	= "-------- LIB END --------";
